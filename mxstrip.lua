#!/usr/bin/env lua

-- mxstrip.lua - Method XML stripper
-- This was written by Tony Finch (dot@dotat.at)
-- You may do anything with it, at your own risk.
-- http://creativecommons.org/publicdomain/zero/1.0/

-- This program is rather more complicated than it needs to be. It
-- builds a DOM-like representation of the input XML file mainly to
-- demonstrate to myself that it isn't unreasonable to expect this to
-- take very little time. Also, writing parsers using LPEG is fun.

local require = require

local io = require "io"
local m = require "lpeg"
local re = require "re"
local table = require "table"

local stdin = io.stdin
local stdout = io.stdout
local stderr = io.stderr
local type = type

local function debug(a,...)
        stderr:write(type(a).."("..tostring(a)..")")
        if select("#",...) == 0 then
                stderr:write("\n")
        else
                stderr:write("\t")
                debug(...)
        end
end

local function printf(string, ...)
        stdout:write(string:format(...))
end

local function die(string, ...)
	error(string:format(...), 0)
end

function push(a,v)
	a[#a+1] = v
end

-----------------------------------------------------------------------------

-- what the parser builds
local dom = { [0] = "document" }

local v = {}
local function p(s)
      return re.compile(s,v)
end

v.WS = m.S " \t\r\n"
v.S0 = v.WS^0
v.S1 = v.WS^1

v.Name = p[[ { [A-Za-z:_] [0-9A-Za-z:._-]* } ]]

v.Entity = p[[ '&' %Name ';' ]] / {
		apos = "'",
		quot = '"',
		amp = '&',
		lt = '<',
		gt = '>',
	}

v.AttrVal = p[[ '"' {~ ([^<&"]+ / %Entity)* ~} '"'  -- '
              / "'" {~ ([^<&']+ / %Entity)* ~} "'"  -- "
             ]]

v.Attrs = p[[ {: %S1 %Name %S0 "=" %S0 %AttrVal :}* ]]

v.TagName = v.Name / function (n) return { [-1] = dom, [0] = n } end

v.InTag = m.Cf(v.TagName * v.Attrs,
	function (elem,attr,val) elem[attr] = val; return elem; end)

v.EmptyTag =  p[[ "<" %InTag %S0 "/>" ]] /
	function (elem) push(dom,elem) end

v.StartTag = p[[ "<" %InTag %S0 ">" ]] /
	function (elem) push(dom,elem) dom = elem; end

v.EndTag = m.Cg(p[[ "</" %Name %S0 ">" ]] * m.Carg(1)) /
	function (name, line)
		if dom[0] ~= name then
			die("Start tag %s does not match"
			    .." end tag %s at line %d\n",
			    dom[0], name, line)
		else
			dom = dom[-1]
		end
	end

v.Content = p[[ {~ ([^<&]+ / %Entity)+ ~} ]] /
	function (text) push(dom,text) end

v.Comment = p[[ '<!--' [^>]* '>' ]]

local Parser = p[[ (%EmptyTag / %StartTag / %EndTag /
                    %Comment / %Content)* {} ]]

local place = 0
local data = ""
for line in io.lines("methods.xml") do
	place = place + 1
	data = data .. line
	local next = Parser:match(data, 1, place)
	if next == nil then
		die("Syntax error at line %d\n", place)
	end
	data = data:sub(next)
end

-----------------------------------------------------------------------------

local method = {}
local method_names = {}
local trigrams = {}

-- this must be kept in sync with the option lists in wbbl.in
local options = {
	-- types
	["Alliance"] = {},
	["Bob"] = {},
	["Delight"] = {},
	["Hybrid"] = {},
	["Place"] = {},
	["Slow Course"] = {},
	["Surprise"] = {},
	["Treble Bob"] = {},
	["Treble Place"] = {},
	["Little"] = {},
	["Little Alliance"] = {},
	["Little Bob"] = {},
	["Little Delight"] = {},
	["Little Hybrid"] = {},
	["Little Place"] = {},
	["Little Surprise"] = {},
	["Little Treble Bob"] = {},
	["Little Treble Place"] = {},
	["Differential"] = {},
	["Differential Alliance"] = {},
	["Differential Bob"] = {},
	["Differential Delight"] = {},
	["Differential Hybrid"] = {},
	["Differential Little"] = {},
	["Differential Place"] = {},
	["Differential Surprise"] = {},
	["Differential Treble Bob"] = {},
	["Differential Treble Place"] = {},
	-- stages
	Singles = {},
	Minimus = {},
	Doubles = {},
	Minor = {},
	Triples = {},
	Major = {},
	Caters = {},
	Royal = {},
	Cinques = {},
	Maximus = {},
	Sextuples = {},
	Fourteen = {},
	Septuples = {},
	Sixteen = {},
	Octuples = {},
	Eighteen = {},
}

local option_matchers = {}

local Alpha = p[[ [A-Za-z] ]]
local Ahpla = -Alpha

for k,v in pairs(options) do
	local pk = m.P(k)
	push(option_matchers, ((1 - pk)^0 * Ahpla * pk * Ahpla) * m.Cc(v))
end

local ascii = m.R"\0\127"
local utf8 = m.R"\192\255" * m.R"\128\191"^1
local char = m.C(ascii + utf8)

local subs_table = {}

-- construct a substitution table from a string consisting of
-- pairs of an original character and its translation
local make_tr_subs = ((char * char) / function (o,s) subs_table[o] = s; end)^0
-- similarly construct a deletion table
local make_del_subs = (char / function (d) subs_table[d] = ""; end)^0

make_tr_subs:match"AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz"
make_tr_subs:match"Éeáaåačcèeéeêeöoøoûuůuṟr²2₃1₁3"
make_del_subs:match[[ (/).,-=?!£'"']]

local subs = m.Cs((char / subs_table)^0)

local function index_method(name,pn)
	method[name] = pn
	push(method_names, name)
	for i,v in ipairs(option_matchers) do
		local t = v:match(name)
		if t then push(t,name) end
	end
	local squashed = subs:match(name)
	for i = 1, (#squashed-2) do
		local trigram = squashed:sub(i,i+2)
		local tri_list = trigrams[trigram]
		if tri_list == nil then
			tri_list = {}
			trigrams[trigram] = tri_list
		end
		push(tri_list, name)
	end
end

-----------------------------------------------------------------------------

local function children_iter(node,index)
	repeat
		index = index + 1
		local child = node[index]
		if child == nil then return nil; end
		if type(child) == "table" then return index, child; end
	until false
end

local function children(node)
	return children_iter, node, 0
end

local function handle_method(node)
	local title, notation
	for i,v in children(node) do
	 	if v[0] == "title" then
			title = v[1]
		elseif v[0] == "notation" then
			notation = v[1]
		end
	end
	index_method(title,notation)
	title = title:gsub("&","&amp;")
	title = title:gsub("'","&apos;")
	title = title:gsub('"',"&quot;")
	printf("<span title='%s'>%s</span>\n", title, notation)
end

local function find_methods(node, name)
	for i,v in children(node) do
		if v[0] == "method"
		then handle_method(v)
		else find_methods(v)
		end
	end
end

-----------------------------------------------------------------------------

stdout = io.output("methods.html")
printf[[
<div id="methods" style="display:none">
<!--
The methods inside this div element were obtained from the following
URL under the licence below.

http://methods.org.uk/method-collections/xml-zip-files/

  These method collections are the copyright of the Central Council of
  Church Bell Ringers. You are welcome to make copies of the material
  for your own use. You may distribute copies to others provided that
  you do not do so for profit and provided that you include this
  copyright statement. If you modify the material before distributing
  it, you must include a clear notice that the material has been
  modified.

The zip files were unpacked and the xml files combined and stripped
down using the script mxstrip.lua.

http://dotat.at/wbbl/mxstrip.lua

The script's output format is HTML. Each method is encoded as a span
element whose title attribute is the method title and whose content is
the method's place notation. These span elements are gathered into a
div element whose id attribute is "methods" and whose style attribue
is "display:none".

This is not strictly a microformat because it is not using HTML markup
to make human-readable text more amenable to computer processing;
instead it is a means for encoding a hidden data structure in HTML.
-->
]]
find_methods(dom)
printf("</div>\n");

-----------------------------------------------------------------------------

stdout = io.output("methods.js")
printf[[
/*
 * The contents of the "method" data structure are derived from data
 * obtained from the following URL under the licence below.
 *
 * http://methods.org.uk/method-collections/xml-zip-files/
 *
 *   These method collections are the copyright of the Central Council
 *   of Church Bell Ringers. You are welcome to make copies of the
 *   material for your own use. You may distribute copies to others
 *   provided that you do not do so for profit and provided that you
 *   include this copyright statement. If you modify the material
 *   before distributing it, you must include a clear notice that the
 *   material has been modified.
 *
 * The zip files were unpacked and the xml files concatenated. Method
 * titles and place notations were extracted from the XML and output
 * with indexes generated by the script mxstrip.lua
 *
 * http://dotat.at/wbbl/mxstrip.lua
 *
 * We use integers to identify methods. These integers index two
 * parallel arrays, method.name and method.pn. The index tables also
 * refer to methods by their integers. The reason for this is to
 * minimise the number of objects (i.e. we don't use an object per
 * method) and to avoid making optimistic assumptions about the
 * efficiency of string handling in the javascript implementation
 * (i.e. we don't use strings to refer to methods in the indexes).
 */
]]
table.sort(method_names)
local method_number = {}
for index,name in ipairs(method_names) do
	method_number[name] = index
end
printf[[
var method = {
  name: [
]]
for i,name in ipairs(method_names) do
    printf("    '%s',\n", name:gsub("'","\\'"))
end
printf[[
  ],
  pn: [
]]
for i,name in ipairs(method_names) do
    printf("    '%s',\n", method[name])
end
printf[[
  ],
  index: {
]]
for tla,array in pairs(trigrams) do
	table.sort(array, function (a,b)
			return method_number[a] < method_number[b]
		end)
	printf("    %s: [", tla)
	for i,name in ipairs(array) do
		printf(" %d,", method_number[name])
	end
	printf("]\n")
end
printf[[
  }
}
]]

-----------------------------------------------------------------------------
