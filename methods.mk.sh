#!/bin/sh

O=methods.mk
ocat() {
	cat >>$O
}
oecho() {
	printf "$@" >>$O
}
cat <<EOF >$O
URL=http://methods.org.uk/method-collections/xml-zip-files/

EOF

FILES="	plain
	surprise
	delight
	trebob
	alliance
	treplace
	hybrid
	principl
	diferntl
	difhuntr
	"

ALL=methods.xml
oecho "$ALL: methods.mk.sh methods.pdf"
for file in $FILES
do oecho " methods/$file.xml"
done
oecho '\n\trm -f %s\n' $ALL
for file in $FILES
do ocat <<EOF
	tail -n +2 methods/$file.xml | sed 's///g' >>$ALL
EOF
done

ocat <<EOF

methods.pdf:
	mkdir -p methods
	ftp \${URL}/method%20xml%201.0.pdf
	mv "method xml 1.0.pdf" methods.pdf
EOF

for file in $FILES
do
	ocat <<EOF

methods/$file-xml.zip:
	cd methods && ftp \${URL}/$file-xml.zip

methods/$file.xml: methods/$file-xml.zip
	cd methods && unzip $file-xml.zip && touch -c $file.xml
EOF
done
